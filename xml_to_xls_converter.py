import xml.etree.ElementTree as ET

FILE = "U_CAN_Ubuntu_18-04_LTS_STIG_V1R1_Manual-xccdf.xml"

with open(FILE) as xml:
    root = ET.fromstring(xml.read())

groups = root.findall('{http://checklists.nist.gov/xccdf/1.1}Group')

# print(groups)

rules = root.findall('{http://checklists.nist.gov/xccdf/1.1}Group/{http://checklists.nist.gov/xccdf/1.1}Rule')

for rule in rules:
    # print(f'{rule.attrib["id"]},{rule.attrib["severity"]},{rule[0].text},{rule[1].text},"{rule[2].text}","{rule[4].text}","{rule[5].text}"')

    print(f'{rule.attrib["id"]},{rule.attrib["severity"]},{rule[0].text},{rule[1].text}' + '"' + f'{rule[2].text}'.replace('"','=').replace(',','=') + '",' + f'{rule[4].text}' + '"' + f'{rule[5].text}'.replace('"','=').replace(',','=') + '",')
    # print('"' + f'{rule[2].text}'.replace('"','=').replace(',','=') + '",')
    # print('"' + f'{rule[5].text}'.replace('"','=').replace(',','=') + '",')
